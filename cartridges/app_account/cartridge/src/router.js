import Vue from 'vue';
import Router from 'vue-router';
import Addresses from './views/Addresses.vue';
import Payments from './views/Payments.vue';
import Landing from './views/Landing.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/landing',
            name: 'landing',
            component: Landing
        },
        {
            path: '/addresses',
            name: 'adresses',
            component: Addresses
        },
        {
            path: '/payments',
            name: 'payments',
            component: Payments
        }
    ]
});
