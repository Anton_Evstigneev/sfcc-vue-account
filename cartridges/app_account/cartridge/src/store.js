import Vue from 'vue';
import Vuex from 'vuex';
import jsonDataUser from './data/jsonDataUser';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: jsonDataUser
    },
    mutations: {
        addressMakeDefault(state, { ID }) {
            state.user.userAddresses.forEach(address => address.default === false);

            var currentAddress = state.user.userAddresses.filter(address => address.ID === ID)[0];
            currentAddress.default = true;
        },
        addressRemove(state, { address }) {
            var addressIndex = state.user.userAddresses.indexOf(address);
            state.user.userAddresses.splice(addressIndex, 1);
        },
        addressSave(state, { addressDetails }) {
            state.user.userAddresses.push(addressDetails);
        },
        paymentMakeDefault(state, { ID }) {
            state.user.userPayments.forEach(payment => payment.default === false);

            var currentPayment = state.user.userPayments.filter(payment => payment.ID === ID)[0];
            currentPayment.default = true;
        },
        paymentRemove(state, { payment }) {
            var paymentIndex = state.user.userPayments.indexOf(payment);
            state.user.userPayments.splice(paymentIndex, 1);
        },
        paymentSave(state, { paymentDetails }) {
            state.user.userPayments.push(paymentDetails);
        }
    },
    actions: {
        addressMakeDefault(context, ID) {
            context.commit('addressMakeDefault', {
                ID
            });
        },
        addressRemove(context, address) {
            context.commit('addressRemove', {
                address
            });
        },
        addressSave(context, addressDetails) {
            context.commit('addressSave', {
                addressDetails
            });
        },
        paymentMakeDefault(context, ID) {
            context.commit('paymentMakeDefault', {
                ID
            });
        },
        paymentRemove(context, payment) {
            context.commit('paymentRemove', {
                payment
            });
        },
        paymentSave(context, paymentDetails) {
            context.commit('paymentSave', {
                paymentDetails
            });
        }
    },
    getters: {
        defaultAddress: (state) => {
            return state.user.userAddresses.filter(address => address.default === true)[0];
        },
        defaultPayment: (state) => {
            return state.user.userPayments.filter(payment => payment.default === true)[0];
        }
    }
});
