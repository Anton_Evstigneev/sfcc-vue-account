module.exports = {
    id: 'backstop_default',
    viewports: [
        {
            label: 'phone',
            width: 320,
            height: 568
        },
        {
            label: 'tablet',
            width: 768,
            height: 1024
        },
        {
            label: 'desktop',
            width: 1280,
            height: 800
        }
    ],
    onBeforeScript: 'puppet/custom/commonOnBeforeScript.js',
    onReadyScript: 'puppet/custom/commonOnReadyScript.js',
    scenarios: [
        {
            label: 'Home page',
            url: 'https://dev18-eshop-celine.demandware.net/s/Celine_US_V2/en_US/home',
            referenceUrl: 'https://dev08-eshop-celine.demandware.net/s/Celine_US_V2/en_US/home',
            readyEvent: '',
            readySelector: '',
            delay: 3000,
            hideSelectors: [
                '.cel-Accordion-background[style*=".gif"]',
                '.cel-Slider-visual'
            ],
            removeSelectors: [],
            hoverSelector: '',
            clickSelector: '',
            postInteractionWait: 0,
            selectors: ['html'],
            selectorExpansion: true,
            expect: 0,
            misMatchThreshold: 1,
            requireSameDimensions: true
        },
        {
            label: 'PDP 185403B27.28BD',
            url: 'https://dev08-eshop-celine.demandware.net/s/Celine_US_V2/en_US/f/handbags/new/medium-scarf-bag-in-printed-calfskin-185403B27.28BD.html',
            referenceUrl: 'https://dev18-eshop-celine.demandware.net/s/Celine_US_V2/en_US/f/handbags/new/medium-scarf-bag-in-printed-calfskin-185403B27.28BD.html',
            readyEvent: '',
            readySelector: '',
            delay: 3000,
            hideSelectors: [
                '.cel-Accordion-background[style*=".gif"]',
                '.cel-Slider-visual'
            ],
            removeSelectors: [],
            hoverSelector: '',
            clickSelector: '',
            postInteractionWait: 0,
            selectors: ['html'],
            selectorExpansion: true,
            expect: 0,
            misMatchThreshold: 1,
            requireSameDimensions: true
        }
    ],
    paths: {
        bitmaps_reference: '~backstop_data/bitmaps_reference',
        bitmaps_test: '~backstop_data/bitmaps_test',
        engine_scripts: '~backstop_data/engine_scripts',
        html_report: '~backstop_data/html_report',
        ci_report: '~backstop_data/ci_report'
    },
    report: ['browser'],
    engine: 'puppeteer',
    engineOptions: {
        args: ['--no-sandbox']
    },
    asyncCaptureLimit: 5,
    asyncCompareLimit: 50,
    debug: false,
    debugWindow: false
};
