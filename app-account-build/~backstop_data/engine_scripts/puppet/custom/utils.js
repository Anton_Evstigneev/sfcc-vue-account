const isOriginalLoggingSuppressed = true;

module.exports = {
    originalConsoleLog: console.log,
    disableLogging() {
        if (isOriginalLoggingSuppressed) {
            console.log = () => {};
        }
    },
    enableLogging() {
        if (isOriginalLoggingSuppressed) {
            console.log = this.originalConsoleLog;
        }
    },
    injectLog(logStr) {
        this.enableLogging();
        console.log(logStr);
        this.disableLogging();
    },
    injectLogCurrentTestInfo(scenario, vp) {
        this.injectLog('SCENARIO > ' + scenario.label + ' (' + vp.label + ')');
    }
};
