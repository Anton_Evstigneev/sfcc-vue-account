// setting NODE_ENV = "test" to indicate that we're runnig tests
require('env-test');
require('mocha-steps');
require('@babel/polyfill');

// initializing google library inside separate jsDOM instance
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

// if you need to test some code with google library this will setup it inside testing environment
global.googleMapAPIKey = 'your_google_api_key';

global.googleApiPromise = new Promise(resolve => {
    const { window } = new JSDOM(`
                <!doctype html>
                <html>
                    <head>
                        <script src="https://maps.googleapis.com/maps/api/js?key=${global.googleMapAPIKey}&libraries=places&callback=googleReady"></script>
                    </head>
                </html>
            `, {
        runScripts: 'dangerously',
        resources: 'usable'
    });

    window.googleReady = () => {
        resolve(window.google);
    };
});

// setup jsDOM global instance to emulate browser environment
require('jsdom-global')();

// adding chai and sinon packages to global object (like window in a browser environment)
// so we don't need to import them in every test module
global.chai = require('chai');
global.sinon = require('sinon');
global.assert = global.chai.assert;
global.vueTestUtils = '@vue/test-utils';

/**
 * @description Allows to properly handle exceptions in async functions during testing
 * @param fn - original Mocha function (second argument in it()/step() methods)
 * @return {Function}
 */
global.mochaAsync = function (fn) {
    return async function (done) {
        const self = this;
        try {
            await fn.call(self);
            done();
        } catch (err) {
            console.log(err.stack);
            done(err);
        }
    };
};