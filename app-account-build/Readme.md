# SFCC SG(SFRA) + any cartridge FD build with Webpack (with FULL Vue.js support)

## Benefits over typical Gulp build (ex SG original):
- much quicker first and incremental builds;
- separated vendor libraries bundle (no need to upload ~2-4mb of bundle code after each build);
- no source maps for vendor libraries- huge increase in build time and site loading speed;
- bundle profiling with GUI report (see what bloats the bundle);
- highly configurable: separate builds for dev, staging, production and profiling;
- ready for testing: environment is already set up- just write you tests (for Vue components too!);
- aliases support (no more relative module imports hell);
- dynamic imports support: Webpack automatically splits bundle into separate files.
 Js in browser automatically downloads specific bundle parts from the server only when they are actually required!
- centralized build (can run regression/unit tests when build finishes, or anything else- you imagine);
- tree shaking;
- nicely documented;

## Commands:
- `npm start` - start development build with watch mode;
- `npm run staging` - start staging build(minified/uglified build with source maps for easy debugging on staging instances);
- `npm run production` - start production build (minified/uglified build without source maps);
- `npm run profiling` - start profiling build (unminified build with source maps for `vendor.js`). After the build 
a browser window will be opened with a visual representation of volume distribution between libs inside `vendor.js`.
- `npm run test` - run Mocha test cases

`Watch` mode is enabled fo development **environment** by default.

## JS files output
Js bundle is split into 3 files (minimum, you could have more if you'll decide to use dynamic imports or several entry points):
- `webpack-runtime.js` - separate file for Webpack runtime - to not include runtime it in each module. Saves space.
IN HTML this file must be included as the first one;
- `vendor.js` - all codebase from "node_modules" ()libraries)- in HTML must be after `webpack-runtime.js` but before `app-test-vue-core-bundle.js`;
- `app-test-vue-core-bundle.js` - your custom code, comes last.
ISML code for lazies:
```html
<script type="text/javascript" src="${URLUtils.staticURL('js/webpack-runtime.js')}" ></script>
<script type="text/javascript" src="${URLUtils.staticURL('js/vendor.js')}" ></script>
<script type="text/javascript" src="${URLUtils.staticURL('js/app-test-vue-core-bundle.js')}" ></script>
```

## Js
- source code has one JS entry point: `app_vue_core/cartridge/js/index.js`;
- in project sources **always** use ES6 import/export syntax so Webpack would be able to "tree shake" the JS codebase:
 this means Webpack will not include module exports that is not used in the source code.
- ES6 async/await and rest/spread operators can be used in main code base and tests.

## Linting
- in `webpack.config.js` there are 2 options to enable/disable linting: `enableJSLinter` and `enableSCSSLinter`;
- JS linter (ESLint) uses `.eslintsrc.json` configuration file;
- SCSS linter (Stylelint)  uses `.stylelintrc.json` configuration file;
- by default linting is done for `development` and `staging` builds;

## Unit testing
- `Mocha` lib is used as test runner. Together with `webpack-mocha`, `jsdom` and `jsdom-global` libraries, it allows to
run tests of any complexity inside fully-emulated browser environment (without actually starting any browser engine).
- `Mocha` unit tests reside in `app_vue_core/cartridge/js/test` directory.
- `Chai` assertion library is used inside `Mocha` tests;
- `mocha-steps` library is used to maintain strict tests ordering even for async test cases;

All testing environment preparations are done in `testsSetup.js`, specifically:
- setting up browser environment emulation with `jsdom` and `jsdom-global` libs;
- adding google library to the global namespace (if needed);
- adding `chai`, `sinon`, `assert` and `vueTestUtils` shortcuts to the global namespace;
- adding mochaAsync function to be able to use async/await syntax in test cases;

## Visual Regression testing with BackstopJS
Allows to visually compare pages on different breakpoints before and after some changes.
Please read some guides at:
https://github.com/garris/BackstopJS#using-backstopjs
https://codeburst.io/visual-regression-testing-with-backstopjs-bc749615191a

BackstopJS data directory: `/cartridges/~backstop_data`
BackstopJS config: `/cartridges/backstop.config.js`

4 npm scripts exist to operate:
- `npm run bt_init` - should be run only once to setup report files;
- `npm run bt_reference` - create(or re-create) reference images of pages using config in `backstop.config.js`;
- `npm run bt_test` - test changes on your SB (or CI after the build) against reference images;
- `npm run bt_approve` - approve new changes: after this new images will become reference images;

## Profiling the bundle
To see what's bloating your bundle code you can use 'profiling' build:
`npm run profiling`
The main issues might be with vendor libraries that are all gathered in `vendor.js`.
When you run tis command a new, un-minified, build with source map for `vendor.js` will be produced. And right 
after that a browser window will open with visual `vendor.js` volume distribution representation.

Current Webpack's production setup outputs the build that is ~70% smaller than the development's one.

## Customizing local builds for each developer
Depending on their preferences each developer can customize the build by renaming `developerBuildPrefs.example.json`
into `developerBuildPrefs.json` and modifying properties:
`enablePostBuildTimer` - displays a post-build timer in the console that displays how many time has passed since last build;
`alwaysGzip` - use gzip bundle compression even for development builds (useful during bad connection times)
`enableAutoUpload` - enables auto-upload to dev sandbox. Setting this to `true` is not enough: if you want to enable auto-uploading of files to your sandbox and Chrome tabs auto-refreshing after each change:
    - rename `dw.example.json` file to `dw.json` and open it for editing.
    - if you're using Eclipse or VScode: disable auto-upload there to avoid files to be uploaded twice;
    - (optional) install a special Chrome plugin for page auto-refreshes:
    `https://chrome.google.com/webstore/detail/tab-reloader/mnbfmcaflfdkgoaaggbpkhchojeinjpm`;
    - in `dw.json` replace corresponding property values with your sandbox configuration data;
    - if you changed something while watch was disabled then run `npm run upload` to sync your changes with the sandbox (this command is the same a making project clean in Eclipse);
    - if you want to make a development build you only need to run `npm start` as usual. This will make a build, upload it to your SB and refresh all related tabs in Chrome on any changed file upload.
    -if you need to make staging/production build you need to run either `npm run staging` or `npm run production` and after the build finished also run `npm run upload`.
